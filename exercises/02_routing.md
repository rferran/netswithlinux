A partir de l'esquema de xarxes i ips de l'aula:


                                                    +------------ internet
                                                    |
                                                +-------+
                       +------------------------+ PROFE +---------------------+
                       |                        +-------+                     |
      2.1.1.0/24       |.254                                 2.1.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.6        |.5        |.4                              |.3        |.2        |.1
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC06 |    |PC05 |    |PC04 |                          |PC03 |    |PC02 |    |PC01 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.2.1.0/24       |.254                                 2.2.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.12       |.11       |.10                             |.9        |.8        |.7
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC12 |    |PC11 |    |PC10 |                          |PC09 |    |PC08 |    |PC07 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.3.1.0/24       |.254                                 2.2.3.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.18       |.17       |.16                             |.15       |.14       |.13
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC18 |    |PC17 |    |PC16 |                          |PC15 |    |PC14 |    |PC13 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.4.1.0/24       |.254                                 2.2.4.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.24       |.23       |.22                             |.21       |.20       |.19
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC24 |    |PC23 |    |PC22 |                          |PC21 |    |PC20 |    |PC19 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.5.1.0/24       |.254                                 2.2.5.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.30       |.29       |.28                             |.27       |.26       |.25
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC30 |    |PC29 |    |PC28 |                          |PC27 |    |PC26 |    |PC25 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         
###### 0. Identifica quin és el teu PC, que ips/màscares ha de portar sobre quines interfícies

        El meu PC és el PC04, la IP és 2.1.1.4/24 i la màscara és: 255.255.255.0.

###### 1. Llista d'ordres per poder introduir manualment les adreces ip del teu lloc de treball amb l'ordre ip i evitar que el NetworkManager o configuracions prèvies interfereixin en aquestes configuracions

        systemctl stop NetworkManager
        dhclient -r
        ip a f dev enp2s0
        ip r f all #route flush
        ip a a 2.1.1.4/24 dev enp2s0
        ip r a default via 2.1.1.1/24 dev enp2s0
        
        echo 1 > /proc/sys/net/ipv4/ip_forward
        
###### 2. Llista d'ordres per configurar el PC del mig de la teva fila com a router amb un comentari explicant el que fa cada línia
        
        systemctl stop NetworkManager
        dhclient -r
        ip a f dev enp2s0
        
        ip a a 2.1.1.5/24 dev enp2s0
        ip a a 2.2.1.11/24 dev enp2s0
        ip a a PROFE dev enp2s0
        

###### 3. Explicació de cada línia de la sortida del comando ip route show 

        [isx45128227@j04 ~]$ ip route show                      # Command
        default via 192.168.0.5 dev enp2s0                      # Switch per el qual sortirem
        proto static  metric 100192.168.0.0/16 dev enp2s0  		# Protocol del enp2s0
        proto kernel  scope link  src 192.168.3.4  metric 100	# Protocol del kernel


###### 4. Fer un ping a un pc de la teva mateixa fila i al PC19 i al PC24. Capturar els paquets amb wireshark i guardar la captura. Analitzar-la observant el camp TTL del protocol IP. Explicar els diferents valors d'aquest camp en funció de la ip a la qual s'ha fet el ping
        
        La meva fila: ping 2.1.1.5
        PC19: ping 2.2.4.19
        PC24: ping 2.4.1.24


###### 5. Fer un traceroute i un mtr al 8.8.8.8 Explicar a partir de que ip es talla i per què.
		
		traceroute 8.8.8.8
		mtr -n --report 8.8.8.8		



###### 6. Explica que és l'emmascarament i com s'aplicaria en l'ordinador del profe per donar sortida a internet

		L'emmascarament de la direcció IP consisteix en que el servidor identifica un dels equips de la xarxa quan intenta 
		connectar a una adreça de fora i és el servidor el que realitza	la petició per sí mateix en comptes de que la màquina clientho faci.
