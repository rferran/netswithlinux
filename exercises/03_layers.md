#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

	# ip r f all
	
	[root@j04 netswithlinux]# ip r f all
	
	
	# ip a f dev enp2s0
	
	[root@j04 netswithlinux]# ip a f dev enp2s0.
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
	
	
	# ip r
	
	[root@j04 netswithlinux]# ip r



Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25
 
	# ip a a 2.2.2.2/24 dev enp2s0
	# ip a a 3.3.3.3/16 dev enp2s0
	# ip a a 4.4.4.4/25 dev enp2s0
	# ip a s dev enp2s0
	
	[root@j04 netswithlinux]# ip a a 2.2.2.2/24 dev enp2s0
	[root@j04 netswithlinux]# ip a a 3.3.3.3/16 dev enp2s0
	[root@j04 netswithlinux]# ip a a 4.4.4.4/25 dev enp2s0
	[root@j04 netswithlinux]# ip a s dev enp2s0
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	3: enp0s18f1u3: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

Consulta la tabla de rutas de tu equipo

	# ip r
	
	[root@j04 netswithlinux]# ip r
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 




Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	# ping 2.2.2.2
	
	[root@j04 netswithlinux]# ping 2.2.2.2
	PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.
	64 bytes from 2.2.2.2: icmp_seq=1 ttl=64 time=0.035 ms
	64 bytes from 2.2.2.2: icmp_seq=2 ttl=64 time=0.043 ms
	64 bytes from 2.2.2.2: icmp_seq=3 ttl=64 time=0.064 ms
	64 bytes from 2.2.2.2: icmp_seq=4 ttl=64 time=0.062 ms
	^C
	--- 2.2.2.2 ping statistics ---
	4 packets transmitted, 4 received, 0% packet loss, time 3000ms
	rtt min/avg/max/mdev = 0.035/0.051/0.064/0.012 ms


	# ping 2.2.2.254
	
	[root@j04 netswithlinux]# ping 2.2.2.254
	PING 2.2.2.254 (2.2.2.254) 56(84) bytes of data.
	From 2.2.2.2 icmp_seq=1 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=2 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=3 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=4 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=5 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=6 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=7 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=8 Destination Host Unreachable
	^C
	--- 2.2.2.254 ping statistics ---
	8 packets transmitted, 0 received, +8 errors, 100% packet loss, time 7000ms

	
	# ping 2.2.5.2
	
	[root@j04 netswithlinux]# ping 2.2.5.2
	connect: Network is unreachable
	
	
	# ping 3.3.3.35
	
	[root@j04 netswithlinux]# ping 3.3.3.35
	PING 3.3.3.35 (3.3.3.35) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=2 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=3 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=4 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=5 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=6 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=7 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=8 Destination Host Unreachable
	^C
	--- 3.3.3.35 ping statistics ---
	8 packets transmitted, 0 received, +8 errors, 100% packet loss, time 7000ms


	# ping 3.3.200.45
	
	[root@j04 netswithlinux]# ping 3.3.200.45
	PING 3.3.200.45 (3.3.200.45) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=2 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=3 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=4 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=5 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=6 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=7 Destination Host Unreachable
	From 3.3.3.3 icmp_seq=8 Destination Host Unreachable
	^C
	--- 3.3.200.45 ping statistics ---
	8 packets transmitted, 0 received, +8 errors, 100% packet loss, time 7000ms


	# ping 4.4.4.8
	
	[root@j04 netswithlinux]# ping 4.4.4.8
	PING 4.4.4.8 (4.4.4.8) 56(84) bytes of data.
	From 4.4.4.4 icmp_seq=1 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=2 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=3 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=4 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=5 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=6 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=7 Destination Host Unreachable
	From 4.4.4.4 icmp_seq=8 Destination Host Unreachable
	^C
	--- 4.4.4.8 ping statistics ---
	9 packets transmitted, 0 received, +8 errors, 100% packet loss, time 8002ms


	# ping 4.4.4.132
	
	[root@j04 netswithlinux]# ping 4.4.4.132
	connect: Network is unreachable



	
#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	# ip r f all
	
	[root@j04 netswithlinux]# ip r f all


	# ip a f dev enp2s0
	
	[root@j04 netswithlinux]# ip a f dev enp2s0
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff




Conecta una segunda interfaz de red por el puerto usb

	# dmesg
	
	[root@j04 netswithlinux]# dmesg
	[ 8776.664323] usb 4-3: new full-speed USB device number 2 using ohci-pci
	[ 8776.811357] usb 4-3: New USB device found, idVendor=0fe6, idProduct=9700
	[ 8776.811369] usb 4-3: New USB device strings: Mfr=0, Product=2, SerialNumber=0
	[ 8776.811376] usb 4-3: Product: USB 2.0 10/100M Ethernet Adaptor
	[ 8777.028581] dm9601 4-3:1.0 eth0: register 'dm9601' at usb-0000:00:12.1-3, Davicom DM96xx USB 10/100 Ethernet, 00:e0:4c:53:44:58
	[ 8777.028954] usbcore: registered new interface driver dm9601
	[ 8777.040892] usbcore: registered new interface driver sr9700
	[ 8777.068041] dm9601 4-3:1.0 enp0s18f1u3: renamed from eth0
	[ 8777.079293] IPv6: ADDRCONF(NETDEV_UP): enp0s18f1u3: link is not ready
	[ 8777.158298] dm9601 4-3:1.0 enp0s18f1u3: link up, 100Mbps, full-duplex, lpa 0xFFFF
	
	
	# ip a
	
	[root@j04 netswithlinux]# ip a
	[...]
	3: enp0s18f1u3: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

Cambiale el nombre a usb0

	# ip link set enp0s18f1u3 down

	[root@j04 netswithlinux]# ip link set enp0s18f1u3 down
	[root@j04 netswithlinux]# ip a
	[...]
	3: enp0s18f1u3: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


	# ip link set enp0s18f1u3 name usb0
	
	[root@j04 netswithlinux]# ip link set enp0s18f1u3 name usb0
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


	# ip link set usb0 up
	
	[root@j04 netswithlinux]# ip link set usb0 up
	[root@j04 netswithlinux]# ip a
	[...]	
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff



Modifica la dirección MAC

	# ip link set usb0 down
	
	[root@j04 netswithlinux]# ip link set usb0 down
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	
	
	# ip link set usb0 address 00:11:22:33:44:55
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff

		
	# ip link set usb0 up

	[root@j04 netswithlinux]# ip link set usb0 up
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff


Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	# ip a a 5.5.5.5/24 dev usb0
	[root@j04 netswithlinux]# ip a a 5.5.5.5/24 dev usb0
	[root@j04 netswithlinux]# ip a
	[...]
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff
		inet 5.5.5.5/24 scope global usb0
		   valid_lft forever preferred_lft forever

	
	# ip a a 7.7.7.7/24 dev enp2s0
	[root@j04 netswithlinux]# ip a a 7.7.7.7/24 dev enp2s0
	[root@j04 netswithlinux]# ip a
	[...]
	
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 192.168.3.4/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 20260sec preferred_lft 20260sec
		inet 7.7.7.7/24 scope global enp2s0
		   valid_lft forever preferred_lft forever

	

Observa la tabla de rutas

	[root@j04 netswithlinux]# ip r
	default via 192.168.0.5 dev enp2s0 
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 
	5.5.5.0/24 dev usb0  proto kernel  scope link  src 5.5.5.5 linkdown 
	7.7.7.0/24 dev enp2s0  proto kernel  scope link  src 7.7.7.7 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.4




#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	# ip r f all
	
	[root@j04 netswithlinux]# ip r f all


	# ip a f dev enp2s0
	
	[root@j04 netswithlinux]# ip a f dev enp2s0


	# ip a 
	[root@j04 netswithlinux]# ip a
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	# ip a a 172.16.99.XX/24
	
	[root@j04 netswithlinux]# ip a a 172.16.99.04 dev enp2s0
	
	
	# ip a 
	
	[root@j04 netswithlinux]# ip a
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff
    inet 172.16.99.4/32 scope global enp2s0
       valid_lft forever preferred_lft forever

	
Lanzar iperf en modo servidor en cada ordenador

	# Servidor

	[root@j04 netswithlinux]# iperf -s
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------



Comprueba con netstat en qué puerto escucha




Conectarse desde otro pc como cliente


	# Client
	
	[root@j04 netswithlinux]# iperf -c SERVER_IP
	


Repetir el procedimiento y capturar los 30 primeros paquetes con tshark




Encontrar los 3 paquetes del handshake de tcp




Abrir dos servidores en dos puertos distintos




Observar como quedan esos puertos abiertos con netstat




Conectarse al servidor con dos clientes y que la prueba dure 1 minuto




Mientras tanto con netstat mirar conexiones abiertas






