# Seguridad en acceso a router mikrotik

Operaciones recomendadas para garantizar la seguridad en la instalación del
router. 

## Cambiamos el puerto donde escucha el ssh

```
/ip service
set ssh port=22340
```

Si queremos cambiar el puerto donde escucha winbox también es buena idea si
queremos acceder por winbox desde internet.

## Permitimos que desde cualquier ip se puedan conectar a los puertos
del router que queramos

```
/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp 
```

## Si quiero añadir esa regla la primera de todas si ya disponemos
 de otras reglas usamos place-before

```
/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp place-before=0
```

## Cambiamos el password (si no tiene no hace falta poner old-password)

```
/password new-password=clase old-password=clase confirm-new-password=clase
```

## Sincronizamos el reloj del router usando el protocolo ntp a 
partir de alguna ip de los servidores de tiempo. En un linux:

```
/system ntp client
set enabled=yes primary-ntp=213.251.52.234 secondary-ntp=158.227.98.15
```

## Script para actualizar la ip dinámica

