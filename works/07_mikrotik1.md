## Com a root

dnf -y install telnet

# Canviem el nom del router

	ip link set enp0s18f1u2 down
	ip link set enp0s18f1u2 name infMKT04
	ip a
	ip link set infMKT04 up
	ip a

# Entrem al router
	
	telnet 192.168.88.1 --> password


dny -y install wine

# Canviem el la ip del router

	systemctl stop NetworkManager.service 
	ip a
	ip a d 192.168.88.254/24 dev infMKT04 --> Trec ip

	ip a a 192.168.88.204/24 dev infMKT04 --> Poso ip nova

	# Comprovem que fa ping
	ip a
	ping 192.168.88.1

## Com a usuari

wget http://download2.mikrotik.com/routeros/winbox/3.0beta3/winbox.exe

wine winbox.exe


###########################################################################


EJERCICIO MIKROTIK 1.

OBJETIVO: FAMILIARIZARSE CON LAS OPERACIONES BÁSICAS EN UN ROUTER MIKROTIK

#1. Entrar al router con telnet, ssh y winbox
	
	# TELNET
	
	telnet 192.168.88.1 --> admin, clase
	
	# SSH
	
	ssh admin@192.168.88.1 --> clase

	# WINBOX
	
	wine winbox.exe --> iniciem sessió: 192.168.88.1 , user: admin , passwd: clase


#2. Como resetear el router
	
	[admin@MikroTik] > system reset-configuration 
	Dangerous! Reset anyway? [y/N]: 
	y
	system configuration will be reset
	Connection closed by foreign host.
	
	o mitjançant el botó reset del router


#3. Cambiar nombre del dispositivo, password 
	
	[admin@infMKT04] > system identity set name=infMKT04
	
	[admin@infMKT04] > password                 
	old-password: *****
	new-password: *****
	confirm-new-password: *****
	

#4. Esquema de configuración de puertos inicial

	[admin@infMKT04] > interface print
	Flags: D - dynamic, X - disabled, R - running, S - slave 
	 #     NAME                                TYPE       ACTUAL-MTU L2MTU
	 0     ether1                              ether            1500  1598
	 1   S ether3                              ether            1500  1598
	 2   S ether4                              ether            1500  1598
	 3  RS infMKT04                            ether            1500  1598
	 4   S wlan1                               wlan             1500  1600
	 5  R  ;;; defconf
		   bridge                              bridge           1500  1598


#5. Cambiar nombres de puertos y deshacer configuraciones iniciales

	[admin@infMKT04] > interface set name= 
	numbers:


#6. Backup y restauración de configuraciones

	[admin@infMKT04] > system backup save name=test  
	Saving system configuration
	Configuration backup saved

	
	[admin@infMKT04] > system backup load name=test.backup 
	password: *****
	Restore and reboot? [y/N]: y
	Restoring system configuration
	System configuration restored, rebooting now
	Connection closed by foreign host.
	
	
	
	
	
	
	
#######
[admin@infMKT04] > /interface ethernet export 
# jan/02/1970 01:07:44 by RouterOS 6.33.5
# software id = 3GAD-YVGP
#
/interface ethernet
set [ find default-name=ether2 ] name=infMKT04
set [ find default-name=ether3 ] master-port=infMKT04
set [ find default-name=ether4 ] master-port=infMKT04


###########

[admin@infMKT04] > /interface bridge port print
Flags: X - disabled, I - inactive, D - dynamic 
 #    INTERFACE                                            BRIDGE                                            PRIORITY  PATH-COST    HORIZON
 0    ;;; defconf
      infMKT04                                             bridge                                                0x80         10       none
 1 I  ;;; defconf
      wlan1    
      
###### WIFI #######  
[admin@infMKT04] > /interface wireless print 
Flags: X - disabled, R - running 
 0    name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge 
      ssid="MikroTik-C389FC" frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 
      vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled 
      default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no 
      security-profile=default compression=no 




#### POSAR SSID WIFI ####

[admin@infMKT04] > /interface wireless set 0 ssid=MKT04
[admin@infMKT04] > /interface wireless set wlan1 ssid=MKT04 
[admin@infMKT04] > /interface wireless print               
Flags: X - disabled, R - running 
 0    name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C3:89:FC arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="MKT04" 
      frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 
      wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes 
      default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 



## CANVIAR CANAL EMISSIÓ

[admin@infMKT04] > /interface wireless set wlan1 ssid=MKT04 frequency=2437


### ANÀLISI DE CANALS

[admin@infMKT04] > /interface wireless spectral-scan                      
number: 0
FREQ  DBM GRAPH                                                                                                                            
2381  -90 :::::::::::::::::::::.                                                                                                           
2384  -87 :::::::::::::::::::::::::::.                                                                                                     
2387  -87 :::::::::::::::::::::::::::.                                                                                                     
2389  -86 ::::::::::::::::::::::::::::::.                                                                                                  
2392  -86 ::::::::::::::::::::::::::::::.                                                                                                  
2395  -87 :::::::::::::::::::::::::::.                                                                                                     
2398  -85 ::::::::::::::::::::::::::::::::.                                                                                                
2401  -67 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.                                                          
2404  -58 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.                                      
2407  -47 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.               
2409  -47 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.               
2412  -47 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.               
2415  -47 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.               
2418  -53 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::...............            
2421  -66 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.............................................          
2424  -81 :::::::::::::::::::::::::::::::::::::::::................................................................................        
2427  -79 ::::::::::::::::::::::::::::::::::::::::::::::.........................................................................          
2429  -65 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::................................                     
2432  -61 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.                                             
2435  -60 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.                                          
2438  -60 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.                                          
2441  -64 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.                                                   
2444  -70 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.                                                                
2447  -80 :::::::::::::::::::::::::::::::::::::::::::.                                                                                     
2449  -84 ::::::::::::::::::::::::::::::::::.                                                                                              
2452  -87 ::::::::::::::::::::::::::::...                                 
[...]



### ANÀLISI XARXES WIFI DISPONIBLES

[admin@infMKT04] > /interface wireless scan          
number: 0
Flags: A - active, P - privacy, R - routeros-network, N - nstreme, T - tdma, W - wds, B - bridge 
       ADDRESS           SSID                           CHANNEL                           SIG   NF SNR RADIO-NAME                          
A R  B 6C:3B:6B:BF:11:4D MKT07                          2412/20-Ce/gn                     -55 -110  55 6C3B6BBF114D                        
AP     E4:AA:5D:5F:55:D0 Docent                         2412/20/gn                        -53 -110  57
AP     E4:AA:5D:5F:55:D2 eduroam                        2412/20/gn                        -53 -110  57
AP     E4:AA:5D:5F:55:D1                                2412/20/gn                        -54 -110  56
A R  B 6C:3B:6B:BC:BF:A5 MKT08                          2412/20-Ce/gn                     -46 -110  64 6C3B6BBCBFA5                        
A R  B 6C:3B:6B:C3:89:F2 infMKT12-ROBERTO               2412/20-Ce/gn                     -46 -110  64 6C3B6BC389F2                        
A R  B 6C:3B:6B:BF:11:2A MikroTik-BF112A                2422/20-Ce/gn                     -65 -114  49 6C3B6BBF112A                        
A R  B 6C:3B:6B:BF:11:3E MikroTik-BF113E                2422/20-Ce/gn                     -53 -114  61 6C3B6BBF113E                        
A R  B 6C:3B:6B:C2:27:0B mktSergi                       2422/20-Ce/gn                     -49 -114  65 6C3B6BC2270B                        
A R  B 6C:3B:6B:C2:27:97 MikroTik-C22797                2422/20-Ce/gn                     -46 -114  68 6C3B6BC22797                        
A R  B 6C:3B:6B:BF:11:39 MikroTik-BF1139                2422/20-Ce/gn                     -57 -114  57 6C3B6BBF1139                        
A R  B 6C:3B:6B:C3:89:93 MKT11                          2422/20-Ce/gn                     -40 -114  74 6C3B6BC38993                        
A R  B 6C:3B:6B:BF:11:52 mkt09                          2422/20-Ce/gn                     -58 -114  56 6C3B6BBF1152                        
A R  B E4:8D:8C:9A:9D:F4 MikroTik-9A9DF4                2422/20-Ce/gn                     -52 -114  62 E48D8C9A9DF4                        
AP     06:27:22:FB:D7:1C edt_profes                     2437/20/gn                        -71 -108  37
AP     06:27:22:FB:D7:E8 edt_profes                     2437/20/gn                        -58 -108  50
AP     0A:27:22:FB:D7:E8 edt_alumnes                    2437/20/gn                        -58 -108  50
AP     0E:27:22:FB:D7:E8 edt_internet                   2437/20/gn                        -59 -108  49
A R  B 6C:3B:6B:BF:10:3A MktXavidise                    2437/20-Ce/gn                     -49 -108  59 6C3B6BBF103A                        
AP     04:62:73:37:2E:50 Docent                         2437/20/gn                        -64 -108  44
AP     04:62:73:37:2E:52 eduroam                        2437/20/gn                        -61 -108  47
AP     04:62:73:37:2E:51                                2437/20/gn                        -67 -108  41

[...]
