Una organización está organizada en dos departamentos:
- deptA
- deptB

Cada departamento ha de estar aislado del otro y ha de poder acceder
a una red de servidores que llamaremos "servers". También se dipone de 
una red wifi que no ha de poder acceder a las redes internas, y que sólo debe dejar
acceder al puerto 80 y 443 del servidor web de la red de servers. Desde
la red wifi también se podrá navegar a internet usando los puertos destino 80 y 443, pero
no se podrán establecer conexiones telnet, ssh o ftp.

Se ha contratado una línea empresarial a un ISP, que ha de ser utilizada
sólo por los servidores, excepto en caso de caída del "router inf" que
es el que se usa habitulamente como router de salida de las redes de 
los departamentos y de los dispositivos conectados por wifi.

Se ha de priorizar el tráfico del deptA respecto al deptB y se ha de 
repartir el ancho de banda por igual entre los usuarios de cada departamento.

Se ha de limitar el tráfico de bajada de la wifi a 500Kbps

El servidor ha de ser accesible desde inernet usando el puerto externo 77XX

Se ha de limitar el acceso por ssh (cambiando el puerto al 12322) al 
router desde cualquier red, y sólo se ha de poder hacer telnet desde la red de control

Se disone de un mikrotik de 4 puertos y de 6 puertos del switch para cada
puesto de trabajo que se conectarán de la siguiente forma:


MIKROTIK                   tagged               untagged   destino 
--------------------------------------------------------------------------
puerto 1   internet        escola,isp                      sw-puerto4 
puerto 2   puerto usb pc                                   pc-usb-ethernet 
puerto 3   redes internas  deptA,deptB,servers             sw-puerto5 
puerto 4      

     
SWITCH                   tagged          untagged type destino
-------------------------------------------------------------------------------------------
puerto 1   deptA                                         deptA      access    PC
puerto 2   detpB                                         deptB      access    PC
puerto 3   servers                                       servers    access    PC
puerto 4   mkt-internet         escola,isp                          trunk     mkt-puerto1
puerto 5   mkt-xarxes_internes  deptA,deptB,servers                 trunk     mkt-puerto3
puerto 6   roseta               deptA,deptB,servers,isp  escola     hybrid    roseta


Tabla de redes, ips y vlans:

REDES      VLAN      NETWORK-ADDR    IP-ADDR-MKT   DHCP-RANGE
---------------------------------------------------------------
deptA      7xx      10.27.1XX.0/24   .1            .100 - .200
deptB      8xx      10.28.1XX.0/24   .1            .100 - .200
servers    9xx      10.29.1XX.0/24   .1            NO DHCP
escola     1000     192.168.0.0/16   .3.2XX        NO DHCP
isp        1005       10.30.0.0/24   .2XX          NO DHCP
wireless   NO-VLAN  10.50.1XX.0/24   .1            .10 - .250
gestion    NO-VLAN 192.168.88.0/24   .1            NO DHCP


El esquema a nivel de capa 3 es el siguiente.


                    +-----------+             XX XXXXX XXXX
                    |router inf +----------+XXXXXX         XX
                    +-----------+        XX                 X
                          |.1            XX   internet      X  +-------------+
                          |               X              XXXX  |  router ISP |
             -------------+----+-+        XXXXXXXXXXXXXXX+-----+             |
               192.168.0.0/16  |                               +-------------+
                               |                                    |.1
                               |             10.30.0.0/24           |
                               |    +-+-----------------------------++
  +------+                     |      |
  |      |              escola |      | isp
  |server|            vlan1000 |      | vlan1005
  |web   |               port1 |      | port1
  +------+               .3.2XX|      |.2xx
     |.100                  +-------------+
     |                      |             |       +))
     |    10.29.1xx.0/24  .1|             |.1     |wireless
+----+----------------------+   MIKROTIK  +-------+
            vlan9xx    port3|             |    10.50.1xx.0/24
            servers         |             |
                            +--+-----+----+
                            .1 |     |  .1
                       vlan7xx |     |  vlan8xx
                         port3 |     |  port3
                         deptA |     |  deptB
                               |     |
                               |     |
      +------------------------+-+   |
          10.27.1xx.0/24           +-+------------------------------+
                                         10.28.1xx.0/24


## Resolución

Pasos:
## 1. Configurar switch

	Switch> enable
	
	Switch# conf t
	
	Switch(config-if)#
	# PORT ACCESS PER DEPTA
		interface fa0/1
		switchport mode access
		switchport access vlan 704
		
	# PORT ACCESS PER DEPTB
		interface fa0/2
		switchport mode access
		switchport access vlan 804

	# PORT ACCESS SERVERS
		interface fa0/3
		switchport mode access
		switchport access vlan 904

	# PORT trunk PER SORTIDA INTERNET MKT
		interface fa0/4
		switchport mode trunk
		switchport trunk allowed vlan 1000,1005

	# PORT trunk PER XARXES INTERNES MKT
		interface fa0/5
		switchport mode trunk
		switchport trunk allowed vlan 704,804,904

	# PORT trunk A ROSETA
		interface fa0/6
		switchport mode access
		switchport access vlan 1000
		exit
		no spanning-tree vlan 2-1005


		interface fa0/7
		switchport mode access
		switchport access vlan 705


		interface fa0/8
		switchport mode access
		switchport access vlan 805


		interface fa0/9
		switchport mode access
		switchport access vlan 905


		interface fa0/10
		switchport mode trunk
		switchport trunk allowed vlan 1000,1005


		interface fa0/11
		switchport mode trunk
		switchport trunk allowed vlan 705,805,905


		interface fa0/12
		switchport mode trunk
		switchport trunk allowed vlan 1005,705,805,905
		switchport access vlan 1000



## 2. Configurar interfaces virtuales y direcciones IP en el router

	# Carreguem la configuració inicial
	
	[admin@mkt04] > system backup load name=20170317_zeroconf.backup 
		password: 
		Restore and reboot? [y/N]: 
		y
		Restoring system configuration
		System configuration restored, rebooting now
		Connection closed by foreign host.
		
		
	# Configurem interfícies virtuals
	
	[admin@mkt04] > interface vlan add vlan-id=704 interface=eth3 name="vnet_deptA"
	[admin@mkt04] > interface vlan add vlan-id=804 interface=eth3 name="vnet_deptB"  
	[admin@mkt04] > interface vlan add vlan-id=904 interface=eth3 name="vnet_servers"      
	[admin@mkt04] > interface vlan add vlan-id=1000 interface=eth1 name="vnet_escola"           
	[admin@mkt04] > interface vlan add vlan-id=1005 interface=eth1 name="vnet_isp"  

	[admin@mkt04] > interface vlan print
	Flags: X - disabled, R - running, S - slave 
	 #    NAME               MTU ARP        VLAN-ID INTERFACE           
	 0    vnet_deptA        1500 enabled        704 eth3                
	 1    vnet_deptB        1500 enabled        804 eth3                
	 2    vnet_escola       1500 enabled       1000 eth1                
	 3    vnet_isp          1500 enabled       1005 eth1                
	 4    vnet_servers      1500 enabled        904 eth3    
	 
	 
	# Posem adreces ip al router
	
	[admin@mkt04] > ip addres add interface=vnet_deptA addres=10.27.104.1/24
	[admin@mkt04] > ip addres add interface=vnet_deptB addres=10.28.104.1/24
	[admin@mkt04] > ip addres add interface=vnet_servers addres=10.29.104.1/24
	[admin@mkt04] > ip addres add interface=vnet_isp addres=10.30.0.204/24
	[admin@mkt04] > ip addres add interface=vnet_escola addres=192.168.3.204/16 
	
	[admin@mkt04] > ip address print 
	Flags: X - disabled, I - invalid, D - dynamic 
	 #   ADDRESS            NETWORK         INTERFACE                   
	 0   ;;; defconf
		 192.168.88.1/24    192.168.88.0    eth2                        
	 1   10.27.104.1/24     10.27.104.0     vnet_deptA                  
	 2   10.28.104.1/24     10.28.104.0     vnet_deptB                  
	 3   10.29.104.1/24     10.29.104.0     vnet_servers                
	 4   10.30.0.204/24     10.30.0.0       vnet_isp                    
	 5   192.168.3.204/16   192.168.0.0     vnet_escola   
	

## 3. Configurar servidores dhcp

	REDES      VLAN      NETWORK-ADDR    IP-ADDR-MKT   DHCP-RANGE
	---------------------------------------------------------------
	deptA      7xx      10.27.1XX.0/24   .1            .100 - .200
	deptB      8xx      10.28.1XX.0/24   .1            .100 - .200
	wireless   NO-VLAN  10.50.1XX.0/24   .1            .10 - .250
		
	# Creem les pools corresponents al router
	
	[admin@mkt04] > ip pool add name=deptApool ranges=10.27.104.100-10.27.104.200
	[admin@mkt04] > ip pool add name=deptBpool ranges=10.28.104.100-10.28.104.200
	[admin@mkt04] > ip pool add name=wireless  ranges=10.50.104.10-10.50.104.250

	[admin@mkt04] > ip pool print
	 # NAME                              RANGES                         
	 0 deptApool                         10.27.104.100-10.27.104.200    
	 1 deptBpool                         10.28.104.100-10.28.104.200    
	 2 wireless                          10.50.104.10-10.50.104.250    
	 
	 
	 # Afegim dhcp server
	 
	 [admin@mkt04] > ip dhcp-server network add dns-server=8.8.8.8 gateway=192.168.0.1 netmask=16 domain=meloinvento.com address=192.168.0.0/16 
	 [admin@mkt04] > ip dhcp-server network add dns-server=8.8.8.8 gateway=10.27.104.1 netmask=24 domain=meloinvento.com address=10.27.104.0/24 
	 [admin@mkt04] > ip dhcp-server network add dns-server=8.8.8.8 gateway=10.28.104.1 netmask=24 domain=meloinvento.com address=10.28.104.0/24
	 [admin@mkt04] > ip dhcp-server network add dns-server=8.8.8.8 gateway=10.50.104.1 netmask=24 domain=meloinvento.com address=10.50.104.0/24
  

    [admin@mkt04] > ip dhcp-server network print 
	 # ADDRESS            GATEWAY         DNS-SERVER      WINS-SERVER     DOMAIN                                                               
	 0 10.27.104.0/24     10.27.104.1     8.8.8.8                         meloinvento.com                                                      
	 1 10.28.104.0/24     10.28.104.1     8.8.8.8                         meloinvento.com                                                                                                           
	 2 10.50.104.0/24     10.50.104.1     8.8.8.8                         meloinvento.com                                                      
	 3 192.168.0.0/16     192.168.0.1     8.8.8.8                         meloinvento.com              
	 
	 
	 # Activem la interfície del wifi
	 
	 [admin@mkt04] > interface enable wlan1 
	 
	 
	 # Afegim adreça ip al wifi
	 
	 [admin@mkt04] > ip address add address=10.50.104.1/24 interface=wlan1 

	 [admin@mkt04] > ip address print                                      
		Flags: X - disabled, I - invalid, D - dynamic 
		 #   ADDRESS            NETWORK         INTERFACE                                                                                          
		 0   ;;; defconf
			 192.168.88.1/24    192.168.88.0    eth2                                                                                               
		 1   10.27.104.1/24     10.27.104.0     vnet_deptA                                                                                         
		 2   10.28.104.1/24     10.28.104.0     vnet_deptB                                                                                         
		 3   10.29.104.1/24     10.29.104.0     vnet_servers                                                                                       
		 4   10.30.0.204/24     10.30.0.0       vnet_isp                                                                                           
		 5   192.168.3.204/16   192.168.0.0     vnet_escola                                                                                        
		 6   10.50.104.1/24     10.50.104.0     wlan1 

	
	 # Afegim servidor a les interfícies
	 
	 [admin@mkt04] >  ip dhcp-server add address-pool=deptApool interface=vnet_deptA
	 [admin@mkt04] >  ip dhcp-server add address-pool=deptBpool interface=vnet_deptB
	 [admin@mkt04] >  ip dhcp-server add address-pool=wireless interface=wlan1

	 # Activem els servidors
	 
	 [admin@mkt04] > ip dhcp-server enable 0
	 [admin@mkt04] > ip dhcp-server enable 1
	 [admin@mkt04] > ip dhcp-server enable 2
	 [admin@mkt04] > ip dhcp-server print   
	 Flags: X - disabled, I - invalid 
	  #   NAME                         INTERFACE                         RELAY           ADDRESS-POOL                         LEASE-TIME ADD-ARP
	  0   dhcp1                        vnet_deptA                                        deptApool                            10m       
	  1   dhcp2                        vnet_deptB                                        deptBpool                            10m       
	  2   dhcp3                        wlan1                                             wireless                             10m       


	# Comprovem que sortim a internet des del PC
	
	[root@j04 ~]# ping 8.8.8.8
	PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
	64 bytes from 8.8.8.8: icmp_seq=1 ttl=54 time=11.7 ms
	64 bytes from 8.8.8.8: icmp_seq=2 ttl=54 time=16.7 ms
	64 bytes from 8.8.8.8: icmp_seq=3 ttl=54 time=10.8 ms
	^C
	--- 8.8.8.8 ping statistics ---
	3 packets transmitted, 3 received, 0% packet loss, time 2003ms
	rtt min/avg/max/mdev = 10.878/13.133/16.732/2.574 ms


## 4. Definir default gateway por router inf y verificar que salen, cambiar
## default gateway por router-isp y verificar que salen. Dejar la ruta con más 
## peso a router-inf. Verificar que al desconectar router-inf se cambia la salida por
## router-isp

	
	# Emmascarem la sortida
	
	[admin@mkt04] > ip firewall nat add chain=srcnat out-interface=vnet_escola action=masquerade 


	# Afegim gateway per sortir per router inf
	
	[admin@mkt04] > ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1 distance=1 check-gateway
	
	# Comprovem que surt a internet
	
	[admin@mkt04] > ping 8.8.8.8
	  SEQ HOST                                     SIZE TTL TIME  STATUS                                                                       
		0 8.8.8.8                                    56  55 10ms 
		1 8.8.8.8                                    56  55 10ms 
		2 8.8.8.8                                    56  55 20ms 
		3 8.8.8.8                                    56  55 10ms 
		sent=4 received=4 packet-loss=0% min-rtt=10ms avg-rtt=12ms max-rtt=20ms 
		
	# Emmascarem la sortida
	
	[admin@mkt04] > ip firewall nat add chain=srcnat out-interface=vnet_isp action=masquerade 	
		
		
	# Afegim gateway per sortir per router isp
	
	[admin@mkt04] > ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 distance=10
	
	
	# Comprovem que surt a internet
	
	[admin@mkt04] > ping 8.8.8.8
	  SEQ HOST                                     SIZE TTL TIME  STATUS                                                                       
		0 8.8.8.8                                    56  55 10ms 
		1 8.8.8.8                                    56  55 10ms 
		2 8.8.8.8                                    56  55 20ms 
		sent=3 received=3 packet-loss=0% min-rtt=10ms avg-rtt=12ms max-rtt=20ms 
		
		
	# Canviem la interfície de la vnet de isp
	
	ip address set [find interface=vnet_isp] interface=vnet_escola


	# Amb traceroute comprovem si podem sortir a internet per els dos gateways
	
	[admin@mkt04] > ip route set numbers=0 disabled=yes
	[admin@mkt04] > tool traceroute 8.8.8.8
	 # ADDRESS                          LOSS SENT    LAST     AVG    BE>
	 1 10.30.0.1                          0%    4   0.4ms     0.8     0>
	 2 10.200.150.1                       0%    4   2.9ms     2.9     2>
	 3 10.10.1.4                          0%    4   0.4ms     0.4     0>
	 4 80.58.67.116                      25%    4   1.5ms     1.5     1>
	 5 81.46.112.21                       0%    4  11.8ms    11.7    11>
	 6 80.58.106.13                       0%    4  10.8ms      12    10>
	 7 176.52.253.97                      0%    4    10ms    10.4      >
	 8 5.53.1.82                          0%    4  10.7ms    10.9    10>
	 9 72.14.232.191                      0%    4  10.8ms    10.9    10>
	10 216.239.48.87                      0%    4  11.4ms    11.5    11>
	11 8.8.8.8                            0%    4  10.7ms    10.9    10>

	[admin@mkt04] > ip route set numbers=0 disabled=no 
	
	[admin@mkt04] > tool traceroute 8.8.8.8           
	 # ADDRESS                          LOSS SENT    LAST     AVG    BE>
	 1 192.168.0.5                        0%    2   0.3ms     1.1     0>
	 2 10.1.1.199                         0%    2   4.1ms     3.5     2>
	 3 10.10.1.4                          0%    2   0.5ms     0.6     0>
	 4 80.58.67.116                       0%    2   1.3ms     1.3     1>
	 5 80.58.88.221                       0%    2  12.5ms    12.6    12>
	 6 80.58.106.13                       0%    2    14ms    14.1      >
	 7                                  100%    2 timeout
	 8 5.53.1.82                          0%    1  18.5ms    18.5    18>
	 9 72.14.234.37                       0%    1  26.8ms    26.8    26>
	10 216.239.50.219                     0%    1  10.6ms    10.6    10>
	11 8.8.8.8                            0%    1  11.1ms    11.1    11>


## 5. Activar las marcas de rutas para que desde la vlan de servers se salga
## por router-isp y desde el resto de redes se salga por router-inf
```
	ip firewall mangle add action=mark-connection chain=prerouting connection-state=new new-connection-mark=fromservers src-address=10.29.104.0/24
	
	ip firewall mangle add action=mark-connection chain=prerouting connection-state=new new-connection-mark=new_to_servers in-interface=vnet_isp dst-address=10.29.104.0/24

	ip firewall mangle add chain=prerouting in-interface=vnet_servers action=mark-connection new-connection-mark=fromservers
	
	ip firewall mangle add chain=prerouting in-interface=vnet_servers connection-mark=fromservers action=mark-routing new-routing-mark=fromservers_toisp
	
	ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 routing-mark=fromservers_toisp

   
```
	[admin@mkt04] > ip firewall mangle print                             
	Flags: X - disabled, I - invalid, D - dynamic                         
	 0  D ;;; special dummy rule to show fasttrack counters
		  chain=prerouting 

	 1  D ;;; special dummy rule to show fasttrack counters
		  chain=forward 

	 2  D ;;; special dummy rule to show fasttrack counters
		  chain=postrouting 

	 3    chain=prerouting action=mark-connection 
		  new-connection-mark=fromservers passthrough=yes 
		  connection-state=new src-address=10.29.104.0/24 log=no 
		  log-prefix="" 

	 4    chain=prerouting action=mark-connection 
		  new-connection-mark=fromservers passthrough=yes 
		  in-interface=vnet_servers log=no log-prefix="" 

	 5    chain=prerouting action=mark-routing 
		  new-routing-mark=fromservers_toisp passthrough=yes 
		  in-interface=vnet_servers connection-mark=fromservers log=no 
		  log-prefix="" 

	 6    chain=prerouting action=mark-connection 
		  new-connection-mark=new_to_servers passthrough=yes 
		  connection-state=new dst-address=10.29.104.0/24 
		  in-interface=vnet_isp log=no log-prefix="" 


## 6. Reglas de firewall para evitar accesos desde deptA a deptB y desde
## wifi solo a server-web y a internet

```
 
	ip firewall filter add chain=forward in-interface=vnet_deptA out-interface=vnet_deptB action=drop 
	
	ip firewall filter add chain=forward in-interface=vnet_deptB out-interface=vnet_deptA action=drop 
	
	ip firewall filter add chain=forward in-interface=wlan1 dst-address=10.29.104.100/24 action=accept
	
	ip firewall filter add chain=forward in-interface=wlan1 out-interface=vnet_deptA action=drop 
	
	ip firewall filter add chain=forward in-interface=wlan1 out-interface=vnet_deptB action=drop 
	
	ip firewall filter add chain=forward in-interface=wlan1 out-interface=vnet_servers action=drop
	
```

	[admin@mkt04] > ip firewall filter print            
	Flags: X - disabled, I - invalid, D - dynamic 
	 0  D ;;; special dummy rule to show fasttrack counters
		  chain=forward 

	 1    ;;; defconf: accept ICMP
		  chain=input action=accept protocol=icmp log=no log-prefix="" 

	 2    ;;; defconf: accept establieshed,related
		  chain=input action=accept 
		  connection-state=established,related log=no log-prefix="" 

	 3    ;;; defconf: drop all from WAN
		  chain=input action=drop in-interface=eth1 log=no 
		  log-prefix="" 

	 4    ;;; defconf: fasttrack
		  chain=forward action=fasttrack-connection 
		  connection-state=established,related log=no log-prefix="" 

	 5    ;;; defconf: accept established,related
		  chain=forward action=accept 
		  connection-state=established,related log=no log-prefix="" 

	 6    ;;; defconf: drop invalid
		  chain=forward action=drop connection-state=invalid log=no 
		  log-prefix="" 

	 7    ;;; defconf:  drop all from WAN not DSTNATed
		  chain=forward action=drop connection-state=new 
		  connection-nat-state=!dstnat in-interface=eth1 log=no 
		  log-prefix="" 

	 8    chain=input action=accept protocol=tcp 
		  src-address=192.168.88.0/24 dst-port=23 log=no log-prefix="" 

	 9    chain=input action=drop protocol=tcp dst-port=23 log=no 
		  log-prefix="" 

	10    chain=forward action=drop in-interface=vnet_deptA 
		  out-interface=vnet_deptB log=no log-prefix="" 

	11    chain=forward action=drop in-interface=vnet_deptB 
		  out-interface=vnet_deptA log=no log-prefix="" 

	12    chain=forward action=accept dst-address=10.29.104.0/24 
		  in-interface=wlan1 log=no log-prefix="" 

	13    chain=forward action=drop in-interface=wlan1 
		  out-interface=vnet_deptA log=no log-prefix="" 

	14    chain=forward action=drop in-interface=wlan1 
		  out-interface=vnet_deptB log=no log-prefix="" 

	15    chain=forward action=drop in-interface=wlan1 
		  out-interface=vnet_servers log=no log-prefix="" 



## 7. Reglas de firewall para que sólo haya telnet desde ips 
## de gestión y servidor ssh por puerto 12322 

	# Canviar port ssh

	[admin@mkt04] > ip service set ssh port=12322

	# Telnet des de ips de gestió
	
	[admin@mkt04] > ip firewall filter add chain=input src-address=192.168.88.0/24 dst-port=23 protocol=tcp action=accept 

	[admin@mkt04] > ip firewall filter add chain=input dst-port=23 protocol=tcp action=drop 



## 8. Limitar ancho de banda de bajada en wireless a 500Kbps

	# Creem la queue i l'assignem a la interfície (parent)

	[admin@mkt04] > queue tree add limit-at=500k max-limit=500k name=queueWIFI packet-mark=no-mark parent=wlan1


## 9. Priorizar tráfico de descarga del deptA respecto a deptB

	# Creem cues per repartir
	
	[admin@mkt04] > queue tree add name=queueREPARTIRdeptA parent=vnet_deptA queue=pcq-download-default packet-mark=no-mark
	[admin@mkt04] > queue tree add name=queueREPARTIRdeptB limit-at=500k max-limit=500k parent=vnet_deptB queue=pcq-download-default packet-mark=no-mark


## 10. Repartir por igual el tráfico entre todos los usuarios de deptA y deptB

	# Creem cues per repartir
	
	[admin@mkt04] > queue tree add name=queueREPARTIRdeptA parent=vnet_deptA queue=pcq-download-default packet-mark=no-mark
	[admin@mkt04] > queue tree add name=queueREPARTIRdeptB parent=vnet_deptB queue=pcq-download-default packet-mark=no-mark






